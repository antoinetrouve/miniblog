.PHONY: help
.DEFAULT_GOAL = help

DOCKER_COMPOSE=@docker-compose
DOCKER_COMPOSE_EXEC=$(DOCKER_COMPOSE) exec
PHP_DOCKER_COMPOSE_EXEC=$(DOCKER_COMPOSE_EXEC) exec php
COMPOSER=$(PHP_DOCKER_COMPOSE_EXEC) php -d memory_limit=-1 /usr/local/bin/composer
SYMFONY_CONSOLE=$(PHP_DOCKER_COMPOSE_EXEC) bin/console

# icons : https://emojipedia.org/
# colors : https://xdevs.com/guide/color_serial/

## _ Docker 🐳 ___________________________________________________________________
start: ## lancer les containers docker
	$(DOCKER_COMPOSE) up -d

stop: ## Arréter les containers docker
	$(DOCKER_COMPOSE) stop

rm: stop ## supprimer les containers docker
	$(DOCKER_COMPOSE) rm -f

restart: rm start Redémarrer les containers docker

ssh-php: ## connexion au container php
	$(PHP_DOCKER_COMPOSE_EXEC) bash


## _ Symfony 🎶 ___________________________________________________________________

vendor-install: ##installaion des vendors
	$(COMPOSER) install

vendor-update: ## Mise à jour des vendors
	$(COMPOSER) update

clean-vendor: ## Suppression du répertoire vendor puis un réinstall
	$(PHP_DOCKER_COMPOSE_EXEC) rm -RF vendor
	$(PHP_DOCKER_COMPOSE_EXEC) rm composer.lock
	$(COMPOSER) install

cc: ## vider le cache
	$(SYMFONY_CONSOLE) c:c

cc-test:   ## vider le cache de l'envirennement test
	$(SYMFONY_CONSOLE) c:c --env=test

cc-hard: ## Supprimer le répertoire chache
	$(PHP_DOCKER_COMPOSE_EXEC) rm -RF var/chache/*

clean-db:  ## Réinitialiser la base de donnée
	- $(SYMFONY_CONSOLE) d:d:d --force --connection             # drop database
	$(SYMFONY_CONSOLE) d:d:c                                    # create database
	$(SYMFONY_CONSOLE) d:m:m --no-interaction                   # doctrine migration
	$(SYMFONY_CONSOLE) d:f:l --no-interaction                   # load fixtures cmd


clean-db-test: cc-hard cc-test ## Réinitialiser la base de donnée en envirennement de test
	- $(SYMFONY_CONSOLE) d:d:d --force --connection --env=test  # drop database
	$(SYMFONY_CONSOLE) d:d:c --env=test                         # create database
	$(SYMFONY_CONSOLE) d:m:m --no-interaction --env=test        # doctrine migration
	$(SYMFONY_CONSOLE) d:f:l --no-interaction --env=test        # load fixtures cmd


test-unit:  ## Lancement des tests unitaire
	$(PHP_DOCKER_COMPOSE_EXEC) bin/phpunit tests/Func/

test-func: clean-db-test ## Lancement des tests fonctionnel
	$(PHP_DOCKER_COMPOSE_EXEC) bin/phpunit tests/Func/

tests: test-func test-unit ## Lancement de tous les tests

cs: ## Lancement du php CS
	$(PHP_DOCKER_COMPOSE_EXEC) vendor/bin/phpcs -n



## _ Others ⚙️ ___________________________________________________________________

help: ## Liste des Commandes
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "  \033[1;33m%-20s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'