[![pipeline status](https://gitlab.com/antoinetrouve/miniblog/badges/develop/pipeline.svg)](https://gitlab.com/antoinetrouve/miniblog/-/commits/develop)
[![coverage report](https://gitlab.com/antoinetrouve/miniblog/badges/develop/coverage.svg)](https://gitlab.com/antoinetrouve/miniblog/-/commits/develop)

# Setup
## Environment
### Make your env ready for dev
1. Create a file name `.env.local`
2. Edit file with information below:
   - APP_ENV=dev
   - APP_SECRET=512ce1db9c1bce166ac7f304542347fd
   - DATABASE_URL="mysql://**@user**:**@password**@**@host**:**@port**/**@database_name**?serverVersion=**@server_version**" (_Replace the information annoted by '@' with your personal information. See .env file for sample_) 
   - CORS_ALLOW_ORIGIN='^https?://(localhost|127\.0\.0\.1)(:[0-9]+)?$'
   - JWT_SECRET_KEY=%kernel.project_dir%/config/jwt/private.pem
   - JWT_PUBLIC_KEY=%kernel.project_dir%/config/jwt/public.pem
   - JWT_PASSPHRASE=39412d571f64bf4a256b9322eeafa1d4
   - JWT_TTL=3600
### Enable profile and debug mode
- `composer require --dev symfony/profiler-pack`
- `composer require --dev symfony/var-dumper`
### Add helper symfony
- `composer require --dev symfony/maker-bundle`
### Create dummies data and Fixtures
- `composer require --dev fakerphp/faker`
- `composer require --dev doctrine/doctrine-fixtures-bundle`
## Database
### Create the database
- `php bin/console doctrine:database:create`
### Create the table and insert dummies data
- `php bin/console doctrine:schema:update`
- `php bin/console doctrine:fixture:load`
### Restart database
- `php bin/console doctrine:schema:drop --full-database --force`
- `php bin/console doctrine:migrations:migrate`
- `php bin/console doctrine:fixtures:load`

# Test
## Make your environment ready for test
1. Create a file name `.env.test.local`
2. Define your global env variables
    - APP_ENV=test
    - KERNEL_CLASS='App\Kernel'
    - APP_SECRET='$ecretf0rt3st'
    - SYMFONY_DEPRECATIONS_HELPER=999999
    - PANTHER_APP_ENV=panther
3. Define your database variable
    - DATABASE_URL="mysql://@user:@password@@host:@port/@database_name?serverVersion=@server_version"
    - Replace the information annotated by '@' with your personal information.
     _See `.env` file for sample_
4. Define your Nelmio CORS configuration
    - CORS_ALLOW_ORIGIN='^https?://(localhost|127\.0\.0\.1)(:[0-9]+)?$'
5. Define Lexik Authentification file
    - JWT_SECRET_KEY=%kernel.project_dir%/config/jwt/private.pem
    - JWT_PUBLIC_KEY=%kernel.project_dir%/config/jwt/public.pem
    - JWT_PASSPHRASE=39412d571f64bf4a256b9322eeafa1d4
    - JWT_TTL=3600
## Launch test
- `php bin/phpunit`

# Docker
## Monter l'image docker
1. Lancer la commande (`up`) permettant de builder (`--build`)en mode détaché (`-d`)
- `docker-compose up -d --build`
2. Liste les conteneurs
- `docker-compose ps`
- Ci-dessous le nom des conteneurs créés :
    - miniblog_mailer_1
    - miniblog_db_1
    - miniblog_php_1
    - miniblog_phpmyadmin_1
3. Modifier votre host local
    - linux : `echo "127.0.0.1 miniblog.local" | sudo tee -a /etc/hosts`
    - windows: `Add-Content $WINDIR\Windows\System32\drivers\etc\hosts "127.0.0.1 miniblog.local"`
## Tester l'installation
1. Sur un navigateur, renseigner l'URL `miniblog.local:9999` qui est l'adresse de notre phpMyAdmin (cf. dockerfile)
    - Vous devriez avoir accès à PhpMyAdmin.
    - Renseigner ensuite l'utilisateur _root_ sans mot de passe pour se connecter.
2. Se connecter au conteneur php
    - Vérifier votre répertoire de travail : `var/www`
    - lister les fichiers disponibles dans votre répertoire de travail. Ceux-ci doivent être identiques à votre projet local.
## Créer l'environnement
- Créer la base de données :
    - `php bin/console doctrine:database:create`
    - `php bin/console doctrine:migrations:migrate`
    - `php bin/console doctrine:fixtures:load`
- Tester l'accès à votre documentation API
    - `http://miniblog.local:8888/api` # cf. dockerfile
## Tips (commande line)
- `docker-compose exec php bash` # Permet de se connecter à notre conteneur php et d'ouvrir une console
- `pwd` # permet de connaître le répertoire de travail du conteneur (lorsque l'on est en mode console)
- `ls -la` # permet de lister le contenu d'un répertoire (lorsque l'on est en mode console)
- `docker-compose exec php ls -la` # permet de lister le contenu d'un répertoire sans lancer une console
- `docker-compose stop` # permet d'arrêter les conteneurs en cours d'exécution
- `docker-compose rm -f` # permet de supprimer les conteneurs
- `docker-compose up -d --build` # permet de monter et d'exécuter docker selon sa configuration
- `docker-compose ps` # permet de lister les conteneurs