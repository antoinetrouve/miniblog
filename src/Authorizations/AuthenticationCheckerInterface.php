<?php

namespace App\Authorizations;

interface AuthenticationCheckerInterface
{
    const ERROR_MESSAGE = 'Need authentication.';

    public function isAuthenticated(): void;
}
