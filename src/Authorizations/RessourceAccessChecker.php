<?php

namespace App\Authorizations;

use App\Exception\RessourceAccessException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class RessourceAccessChecker implements RessourceAccessCheckerInterface
{
    private ?UserInterface $user;

    public function __construct(Security $security)
    {
        $this->user = $security->getUser();
    }

    public function canAccess(?int $id): void
    {
        if ($this->user->getId() !== $id) {
            throw new RessourceAccessException(Response::HTTP_UNAUTHORIZED, self::ERROR_MESSAGE);
        }
    }
}
