<?php

namespace App\Authorizations;

interface RessourceAccessCheckerInterface
{
    const ERROR_MESSAGE = 'Not your ressource.';

    public function canAccess(?int $id): void;
}
