<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    const DEFAULT_USER = ['email' => 'antoinet@gmail.com', 'password' => 'password'];

    private UserPasswordEncoderInterface $encoder;

    private LoggerInterface $logger;

    public function __construct(UserPasswordEncoderInterface $encoder, LoggerInterface $logger)
    {
        $this->encoder = $encoder;
        $this->logger = $logger;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        // create a default user
        $defaultUser = new User();
        $encoded = $this->encoder->encodePassword($defaultUser, self::DEFAULT_USER['password']);
        $defaultUser
            ->setEmail(self::DEFAULT_USER['email'])
            ->setPassword($encoded);

        // Persist
        $manager->persist($defaultUser);

        for ($i = 0; $i < 10; ++$i) {
            $user = new User();
            $encoded = $this->encoder->encodePassword($user, 'password');
            $user
                ->setEmail($faker->email)
                ->setPassword($encoded);

            if (0 === $i % 3) {
                $user
                    ->setStatus(false)
                    ->setAge(23);
            }

            // Persist
            $manager->persist($user);

            try {
                for ($i = 0; $i < random_int(5, 15); ++$i) {
                    $article = (new Article())->setAuthor($user)
                        ->setContent($faker->text(300))
                        ->setName($faker->text(50));

                    // Persist
                    $manager->persist($article);
                }
            } catch (\Exception $e) {
                $this->logger->error('Cannot create article for '.$user->getEmail().'!');
            }
        }

        $manager->flush();
    }
}
