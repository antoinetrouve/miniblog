<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;

class JWTCreatedListener
{
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        // See https://jwt.io/
        $payload = $event->getData();
        $payload['createdAt'] = $event->getUser()->getCreatedAt();
        $event->setData($payload);
    }
}
