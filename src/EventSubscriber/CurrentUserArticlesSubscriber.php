<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Article;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

class CurrentUserArticlesSubscriber implements EventSubscriberInterface
{
    private Security $security;

    /**
     * CurrentUserArticlesSubscriber constructor.
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function onCurrentUserArticles(ViewEvent $event)
    {
        $article = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if ($article instanceof Article && Request::METHOD_POST == $method) {
            $article->setAuthor($this->security->getUser());
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['onCurrentUserArticles', EventPriorities::PRE_VALIDATE],
        ];
    }
}
