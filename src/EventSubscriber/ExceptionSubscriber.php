<?php

namespace App\EventSubscriber;

use App\DependencyInjection\Compiler\ExceptionNormalizerPass;
use App\Normalizer\NormalizerInterface;
use App\Service\ExceptionNormalizerInterface;
use Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;

class ExceptionSubscriber implements EventSubscriberInterface
{
    private static array $normalizers;

    private SerializerInterface $serializer;

    private ExceptionNormalizerInterface $exceptionNormalizer;

    public function __construct(SerializerInterface $serializer, ExceptionNormalizerInterface $exceptionNormalizer)
    {
        $this->serializer = $serializer;
        $this->exceptionNormalizer = $exceptionNormalizer;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [['onProcessException', 0]],
        ];
    }

    public function onProcessException(ExceptionEvent $event)
    {
        $result = null;
        /** @var Exception $exception */
        $exception = $event->getThrowable();
        /** @var NormalizerInterface $normalizer */
        foreach (self::$normalizers as $key => $normalizer) {
            if ($normalizer->support($exception)) {
                $result = $normalizer->normalize($exception);
                break;
            }
        }

        if (null === $result) {
            $result = $this->exceptionNormalizer->format($exception->getMessage());
        }

        $response = new JsonResponse($result, $result['code']);
        $event->setResponse($response);
    }

    /**
     * @see ExceptionNormalizerPass::process()
     */
    public function addNormalizer(NormalizerInterface $normalizer)
    {
        self::$normalizers[] = $normalizer;
    }
}
