<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PasswordEncoderSubscriber implements EventSubscriberInterface
{
    private UserPasswordEncoderInterface $encoder;

    /**
     * PasswordEncoderSubscriber constructor.
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function onEncodePassword(ViewEvent $event)
    {
        $user = $event->getControllerResult();
        if ($user instanceof User) {
            $passHash = $this->encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($passHash);
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.view' => ['onEncodePassword', EventPriorities::PRE_WRITE],
        ];
    }
}
