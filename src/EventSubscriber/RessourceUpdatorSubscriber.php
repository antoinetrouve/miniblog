<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Article;
use App\Entity\User;
use App\Service\RessourceUpdatorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;

class RessourceUpdatorSubscriber implements EventSubscriberInterface
{
    private RessourceUpdatorInterface $ressourceUpdatorSubscriber;

    /**
     * RessourceUpdatorSubscriber constructor.
     */
    public function __construct(RessourceUpdatorInterface $ressourceUpdatorSubscriber)
    {
        $this->ressourceUpdatorSubscriber = $ressourceUpdatorSubscriber;
    }

    public function onCheck(ViewEvent $event)
    {
        $object = $event->getControllerResult();

        if ($object instanceof User || $object instanceof Article) {
            $user = $object instanceof User ? $object : $object->getAuthor();
            $canProcess = $this->ressourceUpdatorSubscriber->process($event->getRequest()->getMethod(), $user);

            if ($canProcess) {
                $user->setUpdatedAt(new \DateTimeImmutable());
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.view' => ['onCheck', EventPriorities::PRE_VALIDATE],
        ];
    }
}
