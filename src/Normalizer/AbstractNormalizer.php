<?php

namespace App\Normalizer;

use App\Service\ExceptionNormalizerInterface;
use Exception;

abstract class AbstractNormalizer implements NormalizerInterface
{
    private array $exceptionTypes;

    protected ExceptionNormalizerInterface $exceptionNormalizer;

    public function __construct(array $exceptionTypes, ExceptionNormalizerInterface $exceptionNormalizer)
    {
        $this->exceptionTypes = $exceptionTypes;
        $this->exceptionNormalizer = $exceptionNormalizer;
    }

    public function support(Exception $exception): bool
    {
        return in_array(get_class($exception), $this->exceptionTypes, true);
    }
}
