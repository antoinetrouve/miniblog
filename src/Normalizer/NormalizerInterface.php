<?php

namespace App\Normalizer;

use Exception;

interface NormalizerInterface
{
    public function normalize(Exception $exception): array;

    public function support(Exception $exception): bool;
}
