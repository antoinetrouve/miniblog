<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Response;

interface ExceptionNormalizerInterface
{
    public function format(string $message, int $statusCode = Response::HTTP_BAD_REQUEST): array;
}
