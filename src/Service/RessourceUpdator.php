<?php

namespace App\Service;

use App\Authorizations\AuthenticationCheckerInterface;
use App\Authorizations\RessourceAccessCheckerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

class RessourceUpdator implements RessourceUpdatorInterface
{
    protected array $methodAllowed = [
        Request::METHOD_PUT,
        Request::METHOD_PATCH,
        Request::METHOD_DELETE,
    ];

    private RessourceAccessCheckerInterface $ressourceAccessChecker;

    private AuthenticationCheckerInterface $authenticationChecker;

    public function __construct(
        RessourceAccessCheckerInterface $ressourceAccessChecker,
        AuthenticationCheckerInterface $authenticationChecker
    ) {
        $this->ressourceAccessChecker = $ressourceAccessChecker;
        $this->authenticationChecker = $authenticationChecker;
    }

    public function process(string $method, UserInterface $user): bool
    {
        if (in_array($method, $this->methodAllowed, true)) {
            $this->authenticationChecker->isAuthenticated();
            $this->ressourceAccessChecker->canAccess($user->getId());

            return true;
        }

        return false;
    }
}
