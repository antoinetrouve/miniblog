<?php

namespace App\Service;

use Symfony\Component\Security\Core\User\UserInterface;

interface RessourceUpdatorInterface
{
    public function process(string $method, UserInterface $user): bool;
}
